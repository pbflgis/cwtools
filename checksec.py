#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logging
import re

import arcgis
import bs4
import cityworks
import requests
import yaml

from base64 import b64decode, b64encode

try:
    from http.client import HTTPConnection
except ImportError:
    from httplib import HTTPConnection

################################################################################

SECURITY_NONE          = 0
SECURITY_GENERATETOKEN = 1
SECURITY_OAUTHUSER     = 2
SECURITY_OAUTHAPP      = 3

SECURITY_TYPE = {
    'none': SECURITY_NONE,
    'generatetoken': SECURITY_GENERATETOKEN,
    'oauthuser': SECURITY_OAUTHUSER,
    'oauthapp': SECURITY_OAUTHAPP,
}

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "\
             "(KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36"

################################################################################

def scrape_inputs(content=None, soup=None):
    if soup is None:
        soup = bs4.BeautifulSoup(content, features='html5lib')

    form = soup.findChild('form')

    if form:
        return { i['name']: i.get('value') for i in form.findAll('input') }

################################################################################

def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', default='config.yaml')
    parser.add_argument('-a', '--authorize', action='store_true')
    parser.add_argument('-f', '--force', action='store_true')
    parser.add_argument('-v', '--verbose', action='count', default=0)
    parser.add_argument('-q', '--quiet', action='store_const', const=0, dest='verbose')
    options = parser.parse_args(args)

    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    log_level = levels[min(options.verbose, len(levels)-1)]

    try:
        import colorlog
        handler = colorlog.StreamHandler()
        handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s[%(name)s] %(message)s'))
        logging.basicConfig(level=log_level, handlers=[handler])
    except ImportError:
        logging.basicConfig(level=log_level)

    ################################################################

    with open(options.config, 'rb') as f:
        config = yaml.load(f, Loader=yaml.SafeLoader)

    CW_CFG = config["cityworks"]
    CW_URL, CW_UID, CW_PWD = CW_CFG["url"], CW_CFG["username"], CW_CFG["password"]
    LOGIN_URL = requests.compat.urljoin(CW_URL, 'Login.aspx')

    svcs = config['services'][0]
    AGP_URL = svcs['connection']['url']
    AGP_UID = svcs['connection']['username']
    AGP_PWD = svcs['connection']['password']

    APP_ID = svcs['client_id']
    APP_SECRET = svcs['client_secret']
    APP_STYPE = SECURITY_TYPE[str(svcs['security']).lower()]

    AGP_TOKEN_URL = AGP_URL + "/sharing/rest/oauth2/token"
    AGP_AUTH_URL = AGP_URL + "/sharing/rest/oauth2/authorize"
    AGP_SIGNIN_URL = AGP_URL + "/sharing/oauth2/signin"

    ################################################################

    session = requests.Session()
    session.headers.update({ 'User-Agent': USER_AGENT })

    rsp = session.get(LOGIN_URL)
    inputs = scrape_inputs(rsp.content.decode())
    logging.debug('inputs = %r', inputs)

    inputs['__EVENTTARGET'] = 'selectedTheme'
    inputs['selectedTheme'] = 'office'
    inputs['txtUserName'] = CW_UID
    inputs['txtPassword'] = CW_PWD

    rsp = session.post(LOGIN_URL, data=inputs)

    ################################################################

    cw = cityworks.API(CW_URL)
    cw.authenticate(CW_UID, CW_PWD)

    sec = { v['ServiceSecurity']['SecurityId']: v for v in
        cw.Ams.GisService.GISServiceEndPointSecurities().Value }

    check = cw.Ams.GisService.ValidateServiceSecurity(
        SecurityIds=list(sec.keys())).Value

    invalid = [k for k, v in check.items() if not v]

    logging.warning('Services with invalid/expired security:\n  - '
        + ('\n  - '.join(sorted(sec[int(k)]['ServiceEndPoint']['ServiceName']
            for k in invalid)) or '(none)'))

    if options.authorize:
        for k in (check.keys() if options.force else invalid):
            k = int(k)
            ep, ss = sec[k]['ServiceEndPoint'], sec[k]['ServiceSecurity']
            logging.info('%d - %s' % (k, ep['ServiceName']))
            cw.Ams.GisService.UpdateGISServiceEndPointSecurity(
                ServiceId = ep['ServiceId'],
                SecurityId = k,
                TokenUrl = AGP_TOKEN_URL,
                AuthorizationUrl = AGP_AUTH_URL,
                ClientId = APP_ID,
                ClientSecret = APP_SECRET,
                SecurityType = APP_STYPE,
                IsPermanent = False,
            )

            redirect_uri = requests.compat.urljoin(cw.url, 'OAuthRedirect.aspx?security_id=%d' % k)
            logging.debug(redirect_uri)

            rsp = session.get(AGP_AUTH_URL, params={
                'response_type': 'code',
                'client_id': APP_ID,
                'redirect_uri': redirect_uri, # 'urn:ietf:wg:oauth:2.0:oob',
                'username': AGP_UID,
                'password': AGP_PWD,
            })

            content = rsp.content.decode()
            state = re.search('"oauth_state":"([-.0-9A-Z_a-z]+)"', content)

            if state:
                state = state.group(1)

                rsp = session.post(AGP_SIGNIN_URL, data={
                    'user_orgkey': '',
                    'username': AGP_UID,
                    'password': AGP_PWD,
                    'oauth_state': state,
                })

            else:
                logging.warning('\tNo OAuth2 state code')
                logging.debug('\n%s\n' % rsp.content)

################################################################################

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])

