#!/usr/bin/env python3

# built-in libraries
import datetime
import logging
import logging.config
import os

# third-party libraries
import cityworks
import yaml

from PIL import Image
from io import BytesIO
import mimetypes

from sources.odbc import OdbcConnection, OdbcSyncSource
from sources.ldap import LdapConnection, LdapSyncSource
from sources.xls import XlsConnection, XlsSyncSource
from sources.csv import CsvConnection, CsvSyncSource

from util import *

################################################################################

class DataSync(object):
    funcs = {}

    def __init__(self, connection, config, can_create=False, can_update=False):
        self.connection = connection
        self.items = bag()
        self.created = bag()
        self.updated = bag()
        self.index = {}
        self.can_create = can_create
        self.can_update = can_update

        self.do_config(config)

    def do_config(self, config):
        pass

    def pull(self): # existing data
        raise NotImplementedError()

    def push(self): # modified data
        raise NotImplementedError()

    def load_items(self, items):
        self.items.clear()
        self.items.update(map(SyncItem, items))
        self.created.clear()
        self.updated.clear()
        self.index.clear()

    def __len__(self):
        return len(self.items)

    def create(self, keyfield, item, **fields):
        if self.can_create:
            item = item.copy()
            item.update(fields)

            logging.debug('create check: %r', item)

            exist = self.lookup(keyfield, item[keyfield])

            if not exist:
                logging.debug('needs to be created!')

                ii = SyncItem({}, item)
                self.created.add(ii)
                self.items.add(ii)

                for k, m in self.index.items():
                    if k in item:
                        m[item[k]] = ii

                return ii

            else:
                logging.debug('already exists')

    def update(self, keyfield, item, **fields):
        if self.can_update:
            item = item.copy()
            item.update(fields)

            logging.debug('update check: %r', item)

            exist = self.lookup(keyfield, item[keyfield])

            if exist:
                changes = {}

                for k, v in item.items():
                    if k not in exist:
                        logging.debug('update: key %r not found in %r', k, exist)
                    if exist.get(k) != v:
                        changes[k] = v

                if changes:
                    logging.debug('needs to be updated!')

                    for k, m in self.index.items():
                        if k in changes:
                            key = exist[k]
                            if m[exist[k]] is exist:
                                del m[exist[k]]
                            m[changes[k]] = exist

                    exist.update(changes)
                    self.updated.add(exist)

                else:
                    logging.debug('nothing to update')

                return changes

            else:
                logging.debug('does not exist')

    def lookup(self, keyfield, key):
        if keyfield not in self.index:
            self.build_index(keyfield)
        return self.index[keyfield].get(key)

    def __getitem__(self, key):
        return self.lookup(*key).item

    def build_index(self, keyfield):
        index = {}
        for item in self.items:
            if keyfield in item:
                key = item[keyfield]
                if key in index:
                    # raise KeyError('duplicate %r key %r' % (keyfield, key))
                    logging.debug('duplicate %r key %r', keyfield, key)
                    continue
                index[key] = item
        self.index[keyfield] = index

    def sync_group(self, source, key_field, create_fields, update_fields):
        loaded_count = 0
        created_count = 0
        updated_count = 0
        skipped_count = 0

        for item in source:
            loaded_count += 1

            key = next(iter(key_field(item)))
            logging.debug('key = %r', key)
            logging.debug('type(item) = %r', type(item))
            cur = item.get(key) or dict()

            if create_fields:
                record = self.create(key, create_fields(item, cur))
                if record:
                    created_count += 1
                    continue

            if update_fields:
                changes = self.update(key, update_fields(item, cur))
                if changes:
                    updated_count += 1
                    continue

            skipped_count += 1

        return loaded_count, created_count, updated_count, skipped_count

    @classmethod
    def sync_types(cls):
        for sub in cls.__subclasses__():
            yield sub

################################################################################

class CustomerSync(DataSync):
    "sync customer data"

    sync_type = 'customers'
    sync_arg = 'C'

    def pull(self):
        ids = self.connection.Ams.CustomerAccount.Search(AcctType=["BUS","RES"]
                                                       , MaxResults=1_000_000)
        accounts = self.connection.Ams.CustomerAccount.ByIds(AcctSids=ids.Value)
        self.load_items(accounts.Value)

    def push(self):
        logging.debug('CustomerAccount.Import: %r'
                    , self.connection.Ams.CustomerAccount.Import(
                        CustomerAccounts=list(dict(item.items()) for item in self.items)
                      , DeleteExisting=True).Value)

################################################################################

class EmployeeSync(DataSync):
    "sync employee data"

    sync_type = 'employees'
    sync_arg = 'L'

    def do_config(self, config):
        paths = config.get('paths', {})
        self.photos_server = paths.get('photos_server')
        self.photos_remote = paths.get('photos_remote', self.photos_server)

        self.funcs = { 'import_photo': self.import_photo }

    def import_photo(self, data, name):
        if data is not None and self.photos_server is not None:

            im = Image.open(BytesIO(data))
            mime = Image.MIME.get(im.format)
            ext = mimetypes.guess_extension(mime)

            fn = name + ext

            upload = os.path.join(self.photos_remote, fn)
            source = os.path.join(self.photos_server, fn)

            try:
                old_size = os.stat(upload).st_size
            except IOError:
                old_size = 0

            if not (self.can_create or self.can_update):
                logging.debug('would save image %s (%s [%s], %dx%d)'
                            , fn, im.format, mime, im.size[0], im.size[1])

            elif len(data) != old_size:
                if (old_size > 0 and self.can_update) or (old_size == 0 and self.can_create):
                    logging.debug('saving image as %s (%s [%s], %dx%d)'
                                , fn, im.format, mime, im.size[0], im.size[1])

                    if not os.path.isdir(self.photos_remote):
                        os.makedirs(self.photos_remote)

                    open(upload, 'wb').write(data)
                else:
                    if old_size > 0:
                        logging.debug('not updating image %s', fn)
                    else:
                        logging.debug('not creating image %s', fn)
            else:
                logging.debug('image with same filesize already exists')

            return source

    def pull(self): # existing data
        employees = self.connection.Ams.Designer.Employees().Value

        self.load_items(employees['DomainEmployees'] + employees['OtherEmployees'])

        users = self.connection.Ams.Designer.Users(IncludeAll=True).Value
        self._users = { u['EmployeeSid']: u for u in users }

    def push(self): # modified data
        for item in self.created:
            try:
                t = self.connection.Ams.Employee.Add(**item)
                e = t.Value

                logging.debug('created employee %d "%s" (%s)',
                              e.get('EmployeeSid'),
                              e.get('LoginName'),
                              e.get('UniqueName'))

                item.update(e)

            except cityworks.api.APIError:
                logging.exception('Error creating employee LoginName:%r', item['LoginName'])

        for item in self.updated:
            sid = item['EmployeeSid']

            if sid == 0: raise RuntimeError("SID is 0")

            try:
                t = self.connection.Ams.Employee.Update(EmployeeSids=[sid], **item)
                e = t.Value[0]

                logging.debug('updated employee %d "%s" (%s)',
                              e.get('EmployeeSid'),
                              e.get('LoginName'),
                              e.get('UniqueName'))

                item.update(e)

            except cityworks.api.APIError:
                logging.exception('Error updating employee EmployeeSid:%d', sid)

        sids = { e['EmployeeSid']: e for e in self.items }

        try:
            pwg = cityworks.PasswordGenerator(self.connection)

            for s, e in sids.items():
                if s not in self._users and e.get('LoginName') and e.get('IsActive'):
                    try:
                        logging.debug('%r', e)
                        t = self.connection.Ams.Designer.CreateUser(
                            EmployeeSid=e.get('EmployeeSid'),
                            Email=e.get('Email'),
                            Username=e.get('LoginName'),
                            Password=pwg.generate(20),
                            Question="Can a match box?",
                            Answer="No, but a tin can!"
                        ).Value

                        logging.debug('created user "%s"', t['LoginName'])

                    except:
                        logging.exception('Error creating user LoginName:%r', e['LoginName'])

        except cityworks.api.APIError:
            logging.exception('Error getting user status')

################################################################################

class EquipmentSync(DataSync):
    "sync equipment data"

    sync_type = 'equipment'
    sync_arg = 'E'

    # major @todo here

################################################################################

class MaterialSync(DataSync):
    "sync materials data"

    sync_type = 'materials'
    sync_arg = 'M'

    def pull(self): # existing data
        materials = self.connection.Ams.Material.All(ViewableOnly=False).Value
        self.load_items(materials)

    def push(self): # modified data
        for item in self.created:
            r = self.connection.Ams.Designer.AddMaterial(**item).Value

            logging.debug('created material %d "%s" (%s)',
                          r.get('MaterialSid'),
                          r.get('Description'),
                          r.get('MaterialUid'))

        for item in self.updated:
            r = self.connection.Ams.Designer.UpdateMaterial(**item).Value

            logging.debug('updated material %d "%s" (%s)',
                          r.get('MaterialSid'),
                          r.get('Description'),
                          r.get('MaterialUid'))

        for item in self.items:
            self.connection.Ams.Storeroom.UpdateStoreroomStock(**item)

################################################################################

_source_types = {
    'ODBC': (OdbcConnection, OdbcSyncSource),
    'LDAP': (LdapConnection, LdapSyncSource),
    'XLS': (XlsConnection, XlsSyncSource),
    'CSV': (CsvConnection, CsvSyncSource),
}

def run_sync(sync, sources, options):
    logging.warning('Syncing: %s', sync.sync_type)

    sync_start_time = datetime.datetime.now()

    if not sources:
        logging.warning('  No sources defined')
        return

    try:
        sync.pull()
        logging.warning('  %d existing', len(sync))
    except NotImplementedError:
        logging.warning('Skipped pulling current "%s" records (not implemented)', sync.sync_type)
    except:
        logging.exception('Failed pulling current "%s" records', sync.sync_type)
        raise

    domains = sync.connection.General.Authentication.Domains().Value

    def domain_id(name):
        l = [d['DomainId'] for d in domains
             if d['DomainName'].upper() == name.upper()]
        return l[0] if l else None

    funcs = FieldMap.funcs.copy()
    funcs.update(domain_id = domain_id)
    funcs.update(sync.funcs)

    loaded_total = 0
    created_total = 0
    updated_total = 0
    skipped_total = 0

    for source in sources:
        info = source['connection'].copy()
        type = info.pop('type').upper()

        logging.warning('  Source: %s', source.get('name', '%s Source' % type))

        with Timer('    Time', log_level=logging.WARNING):
            conn_type, source_type = _source_types[type]
            connection = conn_type(**info)

            for i, group in enumerate(source['groups']):
                data_source = source_type(connection=connection, **group)

                logging.warning('    Group: %s', group.get('name', 'Group %d' % (i+1)))

                with Timer('      Group Time', log_level=logging.WARNING):
                    fields = group.get('fields')
                    key_field = fields.get('key')
                    display_field = fields.get('display')
                    create_fields = fields.get('create')
                    update_fields = fields.get('update')

                    if not display_field:
                        display_field = key_field

                    if create_fields:
                        create_fields.update(key_field)
                        create_fields.update(update_fields)
                    else:
                        logging.warning('    No fields to create for this group')

                    if update_fields:
                        update_fields.update(key_field)
                    else:
                        logging.warning('    No fields to update for this group')

                    key_field = FieldMap(key_field, funcs)
                    display_field = FieldMap(display_field, funcs)
                    create_fields = FieldMap(create_fields, funcs)
                    update_fields = FieldMap(update_fields, funcs)

                    loaded_count, created_count, updated_count, skipped_count = \
                        sync.sync_group(data_source, key_field, create_fields, update_fields)

                    logging.warning('      %s', ', '.join(
                        '%d %s' % (count, label) for label, count in [
                            ('loaded', loaded_count),
                            ('to create' if options.preview else 'created', created_count),
                            ('to update' if options.preview else 'updated', updated_count),
                        ] if count) or None)

                    loaded_total += loaded_count
                    created_total += created_count
                    updated_total += updated_count
                    skipped_total += skipped_count

    logging.warning('  Total: %s', ', '.join(
        '%d %s' % (total, label) for label, total in [
            ('to create' if options.preview else 'created', len(sync.created)),
            ('to update' if options.preview else 'updated', len(sync.updated)),
        ]) or None)

    key = next(iter(key_field.filters.keys()))
    disp = sorted(display_field.filters.keys())

    logging.debug('key = %r', key)
    logging.debug('disp = %r', disp)

    logging.debug('sync.created = %r', list(sync.created))
    logging.debug('sync.updated = %r', list(sync.updated))

    for item in sorted(sync.created, key = lambda i: i.get(key, '')):
        try:
            logging.debug('item = %r', item)
            logging.info('    Create %s', ' '.join('%s:%r' % (k, item[k]) for k in disp))
            for k, v in sorted(item.items()):
                logging.info('      %s: %r', k, v)
        except KeyError:
            logging.debug('item = %r', item)
            logging.exception('Key field not present')

    for item in sorted(sync.updated, key = lambda i: i.get(key, '')):
        changes = list(item.changes())
        if changes:
            logging.info('    Update %s', ' '.join('%s:%r' % (k, item[k]) for k in disp))
            for k, o, v in sorted(changes):
                logging.info('      %s: %r => %r', k, o, v)

    try:
        if options.preview:
            logging.info('  Skipped pushing updated "%s" records (preview mode)', sync.sync_type)
        else:
            sync.push()
    except NotImplementedError:
        logging.info('  Skipped pushing updated "%s" records (not implemented)', sync.sync_type)
    except:
        logging.exception('  Failed pushing updated "%s" records', sync.sync_type)
        raise

    sync_end_time = datetime.datetime.now()
    logging.warning('  Time: %s', sync_end_time - sync_start_time)

################################################################################

def main(options):
    with open(options.file, 'rb') as f:
        config = yaml.load(f, Loader=yaml.SafeLoader)

    try:
        import colorlog
        handler = colorlog.StreamHandler()
        handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s[%(name)s] %(message)s'))
    except ImportError as ex:
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter('%(message)s'))
        logging.exception('cannot import colorlog')

    verbose_levels = [logging.ERROR, logging.WARN, logging.INFO, logging.DEBUG]
    level = verbose_levels[min(options.verbose, len(verbose_levels)-1)]
    handler.setLevel(level)

    ## N.b with Python 3.8 assignment expressions, the below could be:
    # if cl := config.get('logging'):
        # if clh := cl.get('handlers'):

    cl = config.get('logging')
    if cl:
        clh = cl.get('handlers')
        if clh:
            for k, d in list(clh.items()):
                if 'filename' in d:
                    s = d['filename']

                    if ('%' in s) or ('$' in s):
                        s = os.path.expandvars(s)
                    if '~' in s:
                        s = os.path.expanduser(s)
                    if ('%' in s):
                        s = datetime.datetime.now().strftime(s)

                    s = os.path.abspath(s)
                    p = os.path.dirname(s)

                    if not os.path.exists(p):
                        try:
                            os.makedirs(p)
                        except IOError:
                            logging.exception('error creating log path')
                            clh.pop(k)
                            continue

                    d.update(filename=s)

        logging.config.dictConfig(config['logging'])

    logging.getLogger().addHandler(handler)
    logging.getLogger().setLevel(logging.NOTSET)

    logging.debug('options: %r', options)

    if options.auto:
        options.create = True
        options.update = True

    if (not options.preview) and (not options.create) and (not options.update):
        options.preview = True
        options.create = True
        options.update = True

    if options.preview:
        logging.warning('Preview mode; no changes will be made')

    cityworks_connection = cityworks.API(**config['cityworks'])

    syncs = list(filter(lambda t: getattr(options, t.sync_type),
                        DataSync.sync_types()))

    if not syncs:
        syncs = list(DataSync.sync_types())

    try:
        with Timer('Total Time', log_level=logging.WARNING):
            for type in syncs:
                sync = type(cityworks_connection
                          , config
                          , can_create=options.create
                          , can_update=options.update)

                try:
                    run_sync(sync, config.get(sync.sync_type), options)

                except KeyboardInterrupt:
                    raise

                except:
                    logging.exception('Failed running sync')
                    continue

    except KeyboardInterrupt:
        logging.warning('Cancelled by user')

################################################################################

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('--auto', '--all', '-a', action='store_true'
                      , help='run automatic full sync (as \'-cu -ELMC\')')
    parser.add_argument('--file', '-f', default='config.yaml'
                      , help='specify sync config file (default: %(default)s)')

    logs = parser.add_argument_group('logging')

    logs.add_argument('--verbose', '-v'
                    , action='count', default=1
                    , help='show more detailed log messages')
    logs.add_argument('--quiet', '-q'
                    , action='store_const', dest='verbose', const=0
                    , help='show only error log messages')

    task = parser.add_argument_group('records')

    task.add_argument('--create', '-c', action='store_true'
                    , help='create missing records')
    task.add_argument('--update', '-u', action='store_true'
                    , help='update existing records')
    task.add_argument('--preview', '-p', '-n', action='store_true'
                    , help='preview created/updated records')

    data = parser.add_argument_group('datasets')

    for sync in DataSync.sync_types():
        data.add_argument('--%s' % sync.sync_type
                        , '-%c' % sync.sync_arg
                        , action='store_true'
                        , help=sync.__doc__)

    options = parser.parse_args()

    main(options)

