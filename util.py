import base64
import collections
import datetime
import itertools
import logging

################################################################################

class Timer(object):
    def __init__(self
               , name=None
               , log_duration=True
               , log_start=False
               , log_stop=False
               , log_level=logging.DEBUG):
        self.name = name or self.__class__.__name__
        self.log_duration = log_duration
        self.log_start = log_start
        self.log_stop = log_stop
        self.log_level = log_level

    def __enter__(self):
        self.start_time = datetime.datetime.now()

        if self.log_start:
            logging.log(self.log_level
                      , '%s start: %s'
                      , self.name
                      , self.start_time)

    def __exit__(self, *exc_info):
        self.stop_time = datetime.datetime.now()
        self.duration = self.stop_time - self.start_time

        if self.log_stop:
            logging.log(self.log_level
                      , '%s stop: %s'
                      , self.name
                      , self.stop_time)

        if self.log_duration:
            logging.log(self.log_level
                      , '%s: %s'
                      , self.name
                      , self.duration)

################################################################################

class FieldMap(object):
    def __init__(self, filters, funcs=None):
        if filters is None: filters = {}
        # self.filters = { f: FilterChain(p, funcs) for f, p in filters.items() }
        self.filters = { f: compile(p, f, 'eval') for f, p in filters.items() }
        if funcs is not None: self.funcs = funcs

    def __bool__(self):
        return bool(self.filters)

    def __call__(self, item, cur=None):
        r = { k: eval(f, self.funcs, { 'item': item, 'current': cur }) for k, f in self.filters.items() }
        r = { k: v.decode() if hasattr(v, 'decode') else v for k, v in r.items() }
        return r

FieldMap.funcs = {
    'null': lambda v=None: None,
    # 'ifnull': lambda v: None if v is not None else v,
    'default': lambda v, d: d if v is None else v,
    'value': lambda d, v: v,
    'field': lambda v, k: v.get(k),
    'upper': lambda v: nullstr(v).upper(),
    'lower': lambda v: nullstr(v).lower(),
    'title': lambda v: nullstr(v).title(),
    'strip': lambda v, s=None: nullstr(v).strip(s),
    'lstrip': lambda v, s=None: nullstr(v).lstrip(s),
    'rstrip': lambda v, s=None: nullstr(v).rstrip(s),
    'capitalize': lambda v: nullstr(v).capitalize(),
    'sentence': lambda v: nullstr(v).capitalize(),
    'left': lambda v, n: v[:int(n)],
    'right': lambda v, n: v[-int(n):],
    'mid': lambda v, m, n: v[int(m):int(m)+int(n)],
    'encode': lambda v: nullstr(v).encode(),
    'decode': lambda v: v.decode(),
    'format': lambda v, f: f % v,
    'hex': lambda v: base64.b16encode(v).decode(),
    'base16': lambda v: base64.b16encode(v).decode(),
    'base32': lambda v: base64.b32encode(v).decode().rstrip('='),
    'base64': lambda v: base64.b64encode(v).decode().rstrip('='),
    'string': str,
    'float': float,
    'int': int,
    'bool': bool,
}

################################################################################

class SyncItem(collections.ChainMap):
    __slots__ = 'original', 'current'

    def __init__(self, original, current=None):
        super().__init__(current or {}, original)

    def changes(self):
        for k, v in self.maps[0].items():
            o = self.maps[1].get(k)
            if v != o: yield k, o, v

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

################################################################################

class bag(object):
    "A set of unique, mutable objects."

    def __init__(self, it=None):
        self._items = dict()
        if it is not None:
            self.update(it)

    def __len__(self):
        return len(self._items)

    def __contains__(self, item):
        return id(item) in self._items

    def __iter__(self):
        return iter(self._items.values())

    def update(self, other):
        self._items.update({id(item): item for item in other})
        return self

    def add(self, item):
        self._items[id(item)] = item

    def remove(self, item):
        del self._items[id(item)]

    def discard(self, item):
        self._items.pop(id(item), None)

    def pop(self):
        del self._items.popitem()[1]

    def clear(self):
        self._items.clear()

    def union(self, other):
        return bag(self).update(other)

    def intersection(self, other):
        return bag(i for i in self if i in other)

    def difference(self, other):
        return bag(i for i in self if i not in other)

    def symmetric_difference(self, other):
        return bag(itertools.chain(
            (i for i in self if i not in other),
            (i for i in other if i not in self)
            ))

    def intersection_update(self, other):
        self._items = self.intersection(other)._items
        return self

    def difference_update(self, other):
        self._items = self.difference(other)._items
        return self

    def symmetric_difference_update(self, other):
        self._items = self.symmetric_difference(other)._items
        return self

    def __or__(self, other):
        return self.union(other)

    def __and__(self, other):
        return self.intersection(other)

    def __sub__(self, other):
        return self.difference(other)

    def __xor__(self, other):
        return self.symmetric_difference(other)

    def __ior__(self, other):
        return self.update(other)

    def __iand__(self, other):
        return self.intersection_update(other)

    def __isub__(self, other):
        return self.difference_update(other)

    def __ixor__(self, other):
        return self.symmetric_difference_update(other)

