import xlrd

################################################################################

class XlsConnection(object):
    def __init__(self, *args, **kwargs):
        self.connection = xlrd.open_workbook(*args, **kwargs)

    def sheet(self, name_or_index):
        try:
            sheet = self.sheet.sheet_by_index(name_or_index)
        except TypeError:
            sheet = self.sheet.sheet_by_name(name_or_index)

        headers = tuple(v.value for v in sheet.row(0))

        for n in range(1, sheet.nrows):
            yield dict(zip(headers, sheet.row(n)))

################################################################################

class XlsSyncSource(object):
    def __init__(self, **options):
        self.sheet = options.pop('sheet')
        self.connection = options.pop('connection')

    def __iter__(self):
        yield from self.connection.sheet(self.sheet)

