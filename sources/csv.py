import csv

################################################################################

class CsvConnection(object):
    def __init__(self, *args, **kwargs):
        self.connection = csv.DictReader(*args, **kwargs)

    def rows(self):
        yield from self.connection

################################################################################

class CsvSyncSource(object):
    def __init__(self, **options):
        filename = options.pop('filename')
        self.connection = options.pop('connection')

    def __iter__(self):
        yield from self.connection

