import ldap3

################################################################################

class LdapConnection(object):
    def __init__(self, *args, **kwargs):
        self.connection = ldap3.Connection(*args, auto_bind=True, **kwargs)

    def search(self, base, filter=None, scope=ldap3.SUBTREE, attrlist='*'):
        for r in self.connection.extend.standard.\
            paged_search(search_base=base
                       , search_filter=filter
                       , search_scope=scope
                       , attributes=attrlist
                       , paged_size=500
                       , generator=True):

            try:
                r = r['raw_attributes']
            except KeyError:
                continue

            r = { k: v[0] if len(v)==1 else v for k, v in r.items() }
            yield r

################################################################################

class LdapSyncSource(object):
    def __init__(self, **options):
        self.base = options.pop('base')
        self.filter = options.pop('filter', None)
        self.scope = options.pop('scope', ldap3.SUBTREE)
        self.attribs = options.pop('attribs', '*')
        self.connection = options.pop('connection')

    def __iter__(self):
        yield from self.connection.search(self.base
                                        , self.filter
                                        , self.scope
                                        , self.attribs)

