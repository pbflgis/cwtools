import pypyodbc

################################################################################

class OdbcConnection(object):
    def __init__(self, *args, **kwargs):
        self.connection = pypyodbc.connect(*args, **kwargs)

    def query(self, query_string):
        with self.connection.cursor().execute(query_string) as cursor:
            yield from cursor

################################################################################

class OdbcSyncSource(object):
    def __init__(self, **options):
        self.query_string = options.pop('query')
        self.connection = options.pop('connection')

    def __iter__(self):
        yield from self.connection.query(self.query_string)

